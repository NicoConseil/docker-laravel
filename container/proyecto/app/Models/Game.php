<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;
    // Game(game_id, player_id, stats, num_jornada_global(dia++), fecha) 
    // // stats = puntos - asistencias - rebDef - rebOf - tapones - robos - turnovers - faltas - min - per
    // // Pertenecen a 1 Player


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'player_id',
        'puntos',
        'asistencias',
        'rebotes_def',
        'rebotes_of',
        'tapones',
        'robos',
        'turnovers',
        'faltas',
        'minutos_jugados',
        'per',
        'num_jornada_global',
        'fecha',
    ];

    // Cada stats de cada juego pertenecen a 1 jugador 
    public function player(){
        return $this->belongsTo(Player::class);
    }

}
