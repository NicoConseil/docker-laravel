<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;
    // Player (player_id, nombre, posicion, equipo_real, valor_mercado, puntos-fantasia-jornada, participaciones, participaciones_ayer, id_api_externa)

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'nombre',
        'posicion',
        'equipo_real',
        'valor_mercado',
        'puntos_fantasia_jornada',
        'participaciones',
        'participaciones_ayer',
        'id_api_externa',
    ];

    // Cada Jugador pertenece a varios equipos
    public function teams(){
        return $this->belongsToMany(Team::class, PlayerLague::class);
    }
}
