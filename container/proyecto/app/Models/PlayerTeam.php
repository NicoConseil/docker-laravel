<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerTeam extends Model
{
    use HasFactory;
    // PLAYER-TEAM(player_id, team_id)

    protected $fillable = ['team_id'];
    
    public $timestamps = false;
}
