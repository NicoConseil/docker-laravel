<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    // Game(game_id, player_id, stats, num_jornada_global(dia++), fecha) 
    // // stats = puntos - asistencias - rebDef - rebOf - tapones - robos - turnovers - faltas - minutos - per
    // // Pertenecen a 1 Player

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->foreignId('player_id')->constrained('players')->cascadeOnUpdate()->cascadeOnDelete();
            $table->tinyInteger('puntos');
            $table->tinyInteger('asistencias');
            $table->tinyInteger('reb_def');
            $table->tinyInteger('reb_of');
            $table->tinyInteger('tapones');
            $table->tinyInteger('robos');
            $table->tinyInteger('turnovers');
            $table->tinyInteger('faltas');
            $table->tinyInteger('minutos');
            $table->tinyInteger('per');
            $table->smallInteger('num_jornada_global');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
