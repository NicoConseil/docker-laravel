<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    // Player (player_id, nombre, posicion, equipo_real, valor_mercado, puntos-fantasia-jornada, participaciones, participaciones_ayer, id_api_externa)

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->smallInteger('posicion');
            // 1(G) - 2(SG) - 3(SF) - 4(PF) - 5(C)
            $table->string('equipo_real');
            $table->integer('valor_mercado')->default(0);
            $table->integer('puntos_fantasia_jornada')->default(0);
            $table->integer('participaciones')->default(0);
            $table->integer('participaciones_ayer')->default(0);
            $table->integer('id_api_externa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
